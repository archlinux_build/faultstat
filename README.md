# faultstat

page fault tracking tool

Read more here: [faultstat](https://github.com/ColinIanKing/faultstat)

Download faultstat package for Archlinux from here:
[faultstat](https://gitlab.com/archlinux_build/faultstat/-/jobs/artifacts/master/browse?job=run-build)